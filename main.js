var app = function() {
  this.windows     = [];
  this.posturl     = localStorage["serverurl"] + localStorage["secretkey"] + '/info';
  this.actionurl   = localStorage["serverurl"] + localStorage["secretkey"] + '/action';
  this.pollseconds = 5;
  this.polltimer;
  this.infojson    = '[]';
  this.infotimer;
  this.getallparam = {populate: true};
  
  chrome.windows.onCreated.addListener( this.wrappedGetWindows() );
  chrome.windows.onRemoved.addListener( this.wrappedGetWindows() );
  chrome.tabs.onCreated.addListener( this.wrappedGetWindows() );
  chrome.tabs.onRemoved.addListener( this.wrappedGetWindows() );
  chrome.tabs.onUpdated.addListener( this.wrappedGetWindows() );
  
  this.initPoll();
  this.initPostInfo();
  
};
app.prototype.die = function() {
  if (this.polltimer)
    clearTimeout(this.polltimer);
  
  if(this.infotimer)
    clearTimeout(this.infotimer);
};
app.prototype.wrappedGetWindows = function() {
  if (this._wrappedgetwindows)
    return this._wrappedgetwindows;
  
  var self = this;
  return this._wrappedgetwindows = function() {
    self.getWindows.apply(self, arguments);
  };
};
app.prototype.getWindows = function() {
  chrome.windows.getAll(this.getallparam, this.wrappedOnGetWindows());
};
app.prototype.wrappedOnGetWindows = function() {
  if (this._onwrappedgetwindows)
    return this._onwrappedgetwindows;
  
  var self = this;
  return this._onwrappedgetwindows = function() {
    self.onGetWindows.apply(self, arguments);
  };
};
app.prototype.onGetWindows = function(windows) {
  this.windows = windows;
};
app.prototype.assembleBrowserInfo = function() {
  var tabs = [];
  
  for(var i = 0, j = this.windows.length; i < j; i++) {
    var window = this.windows[i];
    
    for(var k = 0, l = window.tabs.length; k < l; k++) {
      var tab = window.tabs[k];
      
      if (tab.url.indexOf('http') !== 0)
        continue;
      
      tabs.push({
        tabid:  tab.id,
        url:    tab.url,
        title:  tab.title || '',
        status: tab.status || '',
      });
    }
  }
  
  return tabs;
  
};
app.prototype.initPoll = function() {
  
  if (this.polltimer)
    clearTimeout(this.polltimer)
  
  this.poll();
  
};
app.prototype.wrappedPoll = function() {
  
  if (this._wrappedpoll)
    return this._wrappedpoll;
  
  var self = this;
  return this._wrappedpoll = function() {
    self.poll.apply(self, arguments);
  };
};
app.prototype.poll = function() {
  var xhr  = new XMLHttpRequest(),
      self = this;
  
  xhr.open("GET", self.actionurl, true);
  xhr.onreadystatechange = function() {
    
    if (xhr.readyState != 4)
      return;
    
    var response;
    try {
      response = JSON.parse(xhr.responseText);
    } catch(e) {
    }
    
    self.polltimer = setTimeout(self.wrappedPoll(), self.pollseconds * 1000);
    self.getAction(response);
    
  }
  
  xhr.send();
  
};
app.prototype.getAction = function(data) {
  
  if (!data || data.status != 'success' || !data.actions || data.actions.length == 0)
    return;
  
  for(var i = 0, j = data.actions.length; i < j; i++){
    var action = data.actions[i];
    var verb   = action.action || '';
    
    switch (verb) {
      case 'closetab':
        this.closeTab(action.tabid);
        break;
      case 'refreshtab':
        this.refreshTab(action.tabid);
        break;
      case 'navigatetab':
        this.navigateTab(action.tabid, action.url);
        break;
    }
  }
  
};
app.prototype.initPostInfo = function() {
  
  if (this.infotimer)
    clearTimeout(this.infotimer);
  
  this.postInfo();
  
};
app.prototype.wrappedPostInfo = function() {
  
  if (this._wrappedpostinfo)
    return this._wrappedpostinfo;
  
  var self = this;
  return this._wrappedpostinfo = function() {
    self.postInfo.apply(self, arguments);
  };
};
app.prototype.postInfo = function() {
  var newinfo = JSON.stringify(this.assembleBrowserInfo()),
      self    = this;
  
  if (self.infojson == newinfo) {
    
    self.infotimer = setTimeout(self.wrappedPostInfo(), self.pollseconds * 1000);
    return;
    
  }
  
  self.infojson = newinfo;
  var xhr   = new XMLHttpRequest();
  var data  = new FormData();
  
  data.append('info', newinfo);
  xhr.open("POST", self.posturl, true);
  xhr.onreadystatechange = function() {
    
    if (xhr.readyState != 4)
      return;
    
    self.infotimer = setTimeout(self.wrappedPostInfo(), self.pollseconds * 1000);
    
  }
  
  xhr.send(data);
  
};
app.prototype.refreshTab = function(tabid) {
  
  if (!tabid)
    return;
  
  chrome.tabs.reload(tabid);
  
};
app.prototype.closeTab = function(tabid) {
  
  if (!tabid)
    return;
  
  chrome.tabs.remove(tabid);
  
};
app.prototype.navigateTab = function(tabid, url) {
  
  if (!tabid || !url)
    return;
  
  chrome.tabs.update(tabid, {url: url}, this.getWindows);
  
};

var application;

chrome.extension.onRequest.addListener(onRequest);
function onRequest(request) {
  switch (request.command) {
    case 'restart':
      
      if (application) {
        application.die();
        application = null;
      }
      
      if (localStorage["secretkey"])
        application = new app();
      
      break;
  }
}

onRequest({command: 'restart'});
