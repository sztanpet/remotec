// TODO 
// ask for permissions for the url as needed? http://developer.chrome.com/extensions/permissions.html
// need to handle removal of permissions too
function saveOptions(e) {
  e.preventDefault();
  var secretkey = document.getElementById('secretkey').value,
      serverurl = document.getElementById('serverurl').value;
  
  if (serverurl.length == 0 || secretkey.length == 0) {
    
    // TODO validate them better
    // TODO should we signal that its invalid to leave it empty? No, I'm not going to call alert()
    return;
    
  }
  
  localStorage["serverurl"] = serverurl;
  localStorage["secretkey"] = secretkey;
  generateURL();
  chrome.extension.sendRequest({command: 'restart'});
  
}

function loadOptions() {
  document.getElementById('secretkey').value = localStorage["secretkey"] || '';
  document.getElementById('serverurl').value =
    localStorage["serverurl"] || 'https://sztanpet.net/remotec/'
  ;
}

function generateURL() {
  var serverurl = ( localStorage["serverurl"] + '/' ).replace(/(.*?)\/+$/, '$1/'),
      secretkey = document.getElementById('secretkey').value,
      urlelem   = document.getElementById('url'),
      url       = serverurl + secretkey + '/';
  
  urlelem.href              = url;
  urlelem.innerText         = url;
  
}

function generateKey(e) {
  
  e.preventDefault();
  
  var string = '';
  for(i = 0; i < 5; ++i) {
    string += '' + Math.random();
  }
  
  document.getElementById('secretkey').value = hex_md5(string);
  
}

document.addEventListener('DOMContentLoaded', function() {
  loadOptions();
  generateURL();
  document.getElementById('optionform').addEventListener('submit', saveOptions);
  document.getElementById('generatekey').addEventListener('click', generateKey);
});
